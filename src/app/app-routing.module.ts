import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CalendarComponent } from './calendar/calendar.component';
import { StudentsComponent } from './students/students.component';

const routes: Routes = [
    { path: 'calendar', component: CalendarComponent },
    { path: 'students', component: StudentsComponent },
    { path: '', redirectTo: 'calendar', pathMatch: 'full' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
