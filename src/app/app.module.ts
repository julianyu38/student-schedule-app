import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomMaterialModule } from './core/material.module';

import { CalendarComponent } from './calendar/calendar.component';
import { StudentsComponent } from './students/students.component';

import { FullCalendarModule } from '@fullcalendar/angular';
import { CreateSelectDlgComponent } from './create-select-dlg/create-select-dlg.component'; 

import { FormsModule } from '@angular/forms';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';

import { HttpClientModule } from '@angular/common/http';
import { BackendService } from './services/backend.service';
import { UtilityService } from './services/utility.service';
import { AddStudentsDlgComponent } from './add-students-dlg/add-students-dlg.component';
@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        CalendarComponent,
        StudentsComponent,
        CreateSelectDlgComponent,
        AddStudentsDlgComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        CustomMaterialModule,
        FullCalendarModule,
        FormsModule,
        NgxMaterialTimepickerModule
    ],
    providers: [BackendService, UtilityService],
    entryComponents:[CreateSelectDlgComponent, AddStudentsDlgComponent],
    bootstrap: [AppComponent]
})
export class AppModule { }
