export interface ISchedule{
    id: number,
    title: string,
    startDate: Date,
    startTime: Date,
    endDate : Date,
    endTime: Date,
    student: string
}